# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=2000
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/aciz/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

setopt prompt_subst

source ~/antigen.zsh

antigen use oh-my-zsh

antigen theme amuse

antigen bundle git
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

antigen apply

export PATH="$HOME/apps/bin:$PATH"

export PATH="$HOME/.local/bin:$PATH"

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
