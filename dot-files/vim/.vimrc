
set nocompatible

syntax enable

"Wcięcia
set tabstop=4
set shiftwidth=4
set autoindent
set smartindent
set expandtab

set backspace=indent,eol,start

colorscheme darkspectrum

set nobackup       "no backup files
set nowritebackup  "only in case you don't want a backup file while editing
set noswapfile     "no swap files

set t_Co=256 "tryb bardziej kolorowy w xtermie

set number
set ignorecase
set hlsearch
set incsearch

set ruler
set cul "podświetlenie wiersza z kursorem

set scrolloff=3 "trzmaj się 3 linijki od końców ekranu

set mouse=a

"skacz między liniami przez koniec i początek
set whichwrap=b,s,h,l,<,>,[,]

set foldmethod=indent "zwijaj na wcięcich

set enc=utf-8
set fileencodings=utf-8,latin2

set spelllang=pl

"podpowiadaj przy szukaniu plików przez menu
set wildmode=longest:full
set wildmenu

"podpowiadanie składni
set completeopt=longest,menuone

set guioptions=aeir "czysty gvim
"set guifont=Consolas:h9:cANSI

let g:airline_detect_whitespace=0

au BufNewFile,BufRead *.less set filetype=less

:filetype plugin on

" KEY MAPPINGS

map <leader>gt :call TimeLapse() <cr>

"uzupełnianie nawiasów!

inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap ] <c-r>=ClosePair(']')<CR>
inoremap } <c-r>=ClosePair('}')<CR>
inoremap " <c-r>=QuoteDelim('"')<CR>
inoremap ' <c-r>=QuoteDelim("'")<CR>

function ClosePair(char)
	if getline('.')[col('.') - 1] == a:char
		return "\<Right>"
	else
		return a:char
	endif
endf

function QuoteDelim(char)

	let line = getline('.')
	let col = col('.')

	if line[col - 2] == "\\"
		"Inserting a quoted quotation mark into the string
		return a:char
	elseif line[col - 1] == a:char
		"Escaping out of the string
		return "\<Right>"
	else
		"Starting a string
		return a:char.a:char."\<Esc>i"
	endif
endf

" Mapping for revealing syntax group

nmap <C-S-P> :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc
