Setup Linux
===========

Set of scripts installing basic tools and their configuration:

* GVim
* Git
* Curl
* Wget
* Terminator
* Zsh

Currently it supports arch based distros and is using ansible to perform setup.

# How to start

```bash
curl -L https://bitbucket.org/SerafAC/linux-setup/raw/master/setup-linux.sh | bash
```

