#!/bin/bash

set -e

echo "#########################################"
echo "# Linux init script "
echo "#########################################"

sudo pacman --needed -S ansible git


mkdir -p $HOME/projects

cd $HOME/projects

if [ ! -d "./linux-setup" ] ; then
    git clone git@bitbucket.org:SerafAC/linux-setup.git
fi

cd linux-setup/playbooks

sudo LC_ALL=C ansible-playbook -v setup-all.yml

